#!/bin/sh
set -eu
cd $HOME
pwd
( set -x; git config --global credential.helper || git config --global --replace-all credential.helper 'cache --timeout 2592000' )


# ping 可否で ssh or https を切り替え
if timeout 5 ping -c 2 gitlab.com; then
    repo_prefix='git@gitlab.com:'  # ssh
else
    repo_prefix='https://gitlab.com/'
fi

if [ ! -d .dot ]; then
  ( set -x; git clone ${repo_prefix:?}k9i/dot.git .dot )
fi

.dot/bin/setup_dot.sh
